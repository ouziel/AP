/**
 * Setup webpack public path
 * to enable lazy-including of
 * js chunks
 *
 */
import './vendor/webpack.publicPath';
/**
 * Your theme's js starts
 * here...
 */
// import methods from './scripts/methods/index.js';
import polyfills from './vendor/polyfills';
import settings from './scripts/settings';
import state from './scripts/state';
import elms from './scripts/elms';
import methods from './scripts/methods';
import cache from './scripts/cache';
import init from './scripts/init';
import flickity from 'flickity/dist/flickity.pkgd.js';
/* eslint no-console: 0 */
// console.log('methods is' + methods);
const App = {
	settings,
	methods,
	elms,
	cache,
	init,
	state
};
App.init();