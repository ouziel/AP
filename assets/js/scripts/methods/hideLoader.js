import elms from '../elms';
const hideLoader = function() {
	elms
		.loader
		.removeClass('loader-active');
}
export default hideLoader;
