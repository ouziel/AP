import $ from 'jquery';
import state from '../state';
const getSingle = function(el) {
	$(document)
		.on('click', function(e) {
			if (state.projects.singleOpen && !$(e.target).hasClass('single_project')) {
				closeSingleBar();
			}
		});
	const zoomed = $(el).attr('data-zoom');
	if (zoomed) {
		$(el).removeAttr('data-zoom', '');
		closeSingleBar();
	} else {
		$('.element-item').removeAttr('data-zoom', '');
		$(el).attr('data-zoom', 'true');
		openSingleBar(el);
		console.log(state);
	}
}
function closeSingleBar() {
	const stage = $('.single_project');
	$(stage).removeClass('single_on');
	state.projects.singleOpen = false;
}
function openSingleBar(el) {
	state.projects.singleOpen = true;
	const stage = $('.single_project');
	const info = $(el).data('info');
	const title = $(el).data('title');
	const img = $(el).data('img');
	const d = $('<div class="single_content"></div>');
	const p = $(`<p>${info}</p>`);
	const h = $(`<h3>${title}</h3>`);
	const i = $(`<img src="${img}"/>`);
	$(d).append(h, p);
	$(stage)
		.addClass('single_on')
		.find('.inner')
		.empty()
		.append(d, i);
	if ($('body').scrollTop() > 410) {
		$('html, body').animate({
			scrollTop: stage
				.offset()
				.top - 150
		}, 500);
	}
}
export default getSingle;
