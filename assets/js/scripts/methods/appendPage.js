import $ from 'jquery';
import changeBodyClasses from './changeBodyClasses';
import toggleLoader from './toggleLoader';
import elms from '../elms';
import initPageTasks from '../routes/initPageTasks';
import settings from '../settings';
const appendPage = function(header, main, classList) {
	$('main').removeClass('shapes');
	changeBodyClasses(classList);
	initPageTasks();
	toggleLoader();
	$('header')
		.slideDown()
		.html($(header).html());
	$('main')
		.slideDown()
		.html($(main).html());
	if (settings.shapes) {
		$('main').addClass('shapes');
	}
}
export default appendPage;