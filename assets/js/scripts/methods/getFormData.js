import $ from 'jquery';
import {log} from './util';
const getFormData = function(form) {
	var data = form.serialize();
	log('FORM SERIALIZED')(data);
	return data;
}
export default getFormData;
