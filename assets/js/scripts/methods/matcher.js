import $ from 'jquery';
class Matcher {
	constructor(options) {
		if (!options || typeof options !== 'object') {
			throw new Error("'options' should be an object");
		}
		this.options = options;
		this.init();
	}
	resize() {
		this.init();
	}
	getLabels(options = this.options) {
		const root = Object.keys(options),
			keys = Object.keys(options[root]);
		return {root, keys}
	}
	init(options = this.options) {
		const t = this;
		const {root, keys} = t.getLabels();
		keys.map(key => {
			let o = options[root][key];
			Object
				.keys(o)
				.map(m => {
					let min,
						max,
						cb;
					if (typeof o[m] === 'object') {
						min = o[m].min;
						max = o[m].max;
						cb = o[m].cb;
					} else {
						min = o.min;
						max = o.max;
						cb = o.cb;
					}
					let min_str = `(min-width: ${min}px) `,
						max_str = `(max-width: ${max}px)`,
						str = min_str;
					if (min && max) {
						str = `${min_str} and ${max_str}`;
					}
					let mql = window.matchMedia(str);
					if (mql.matches) {
						console.log('Matched Media Query for: ' + key);
						cb(key);
					}
				})
		})
	}
}
export default Matcher;
