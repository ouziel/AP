import $ from 'jquery';
function search(data, cb) {
	$.post({
		url: wpapp.ajaxurl,
		data: data + '&action=ap_search',
		success: function success(response) {
			const json = JSON.parse(response);
			cb(json);
		},
		error: function error(xhr, status, _error2) {
			log('SEARCH FAILED', 'warn')({error: _error2, response: xhr.responseText});
		}
	});
}
export default search;