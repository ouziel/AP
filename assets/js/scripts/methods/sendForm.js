import $ from 'jquery';
import getFormData from './getFormData';
import hideForm from './hideForm';
import elms from '../elms';
import showCompletionMsg from './showCompletionMsg';
import {log} from './util';
const sendForm = function(e) {
	e.preventDefault();
	const self = this,
		data = getFormData(elms.form),
		isDataValid = $(elms.formid).valid();
	if (isDataValid) {
		log('FORM VALID', 'OK');
		$.post({
			url: wpapp.ajaxurl,
			data: data + '&action=ap_sendForm',
			success: function success(response) {
				log('FORM RESPONSE', 'OK');
				hideForm(elms.form);
				showCompletionMsg(elms.msg);
			},
			error: function error(xhr, status, _error2) {
				log('FORM FAILED', 'warn')({error: _error2, response: xhr.responseText});
			}
		});
	}
}
export default sendForm;