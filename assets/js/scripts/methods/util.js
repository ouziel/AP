import settings from '../settings';
const util = {
	log: function log(x, type) {
		if (!type) {
			type = 'default';
		}
		var bg;
		switch (type.toUpperCase()) {
			case 'WARN':
				bg = 'red';
				break;
			case 'BOOL':
				bg = 'blue';
				break;
			case 'OK':
				bg = 'GREEN';
				break;
			case 'default':
				bg = '#222';
				break;
			default:
				bg = '#222';
		}
		console.log('%c ' + x + ' ', 'background: ' + bg + '; color: #fff');
		return function(next) {
			console.log(next);
		};
	},
	isMobile: function() {
		var W = $(window).width(),
			H = $(window).height();
		if (W > settings.mobileMaxWidth) {
			return false;
		} else {
			return true;
		}
	},
	smoothScroll: function(el) {
		console.log('scrollTop');
		const scrollTop = $(el)
			.offset()
			.top - 250;
		$('body').animate({
			scrollTop: scrollTop
		}, 500);
	}
};
const {log, isMobile, smoothScroll} = util;
export {util as default, log, isMobile, smoothScroll}
