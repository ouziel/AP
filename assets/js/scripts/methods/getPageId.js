const getPageId = function() {
	let classes = document.body.classList,
		str = 'page-id-',
		id = -1;
	for (var i = 0; i < classes.length; i++) {
		if (classes[i].indexOf(str) > -1) {
			let cls = classes[i];
			id = cls.substring(str.length);
		}
	}
	return id;
}
export default getPageId;
