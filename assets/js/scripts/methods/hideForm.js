import $ from 'jquery';
const hideForm = function(form) {
	$(form).slideUp();
}
export default hideForm;
