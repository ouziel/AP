import transformicons from '../../vendor/tcons';
import elms from '../elms'
const toggleNavBtn = function() {
	transformicons.toggle(elms.toggleBtn);
}
export default toggleNavBtn;
