import $ from 'jquery';
const showCompletionMsg = function(msg) {
	setTimeout(function() {
		$(msg).fadeIn();
		$(msg)
			.find('h5')
			.animate({
				opacity: 1,
				top: '+=50'
			}, 1000);
	}, 200);
}
export default showCompletionMsg;
