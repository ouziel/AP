import elms from '../elms';
const toggleLoader = function() {
	elms
		.loader
		.toggleClass('loader-active');
}
export default toggleLoader;
