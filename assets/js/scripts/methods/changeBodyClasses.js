import {isMobile} from './util';
const changeBodyClasses = function(classList) {
	const classes = Array
		.from(classList)
		.join(' ');
	$('body')
		.removeClass()
		.addClass(classes);
};
export default changeBodyClasses;
