import $ from 'jquery';
import toggleLoader from './toggleLoader';
import removeContent from './removeContent';
import toggleNav from './toggleNav';
import toggleNavBtn from './toggleNavBtn';
import getPageName from './getPageName';
import appendPage from './appendPage';
import s from '../settings';
import {log} from './util';
const getPage = function(e, url) {
	e.preventDefault();
	if (s.ajaxRequest)
		return false;
	s.ajaxRequest = true;
	let cache = true;
	if (s.isMobile) {
		if ($(e.target).hasClass('get_page_ajax')) {
			toggleNav();
			toggleNavBtn();
		}
		cache = false;
	}
	const link = url || e
			.target
			.getAttribute('href'),
		name = getPageName(link);
	log('FETCHING PAGE')(name);
	toggleLoader();
	removeContent();
	$.ajax({
		type: 'GET',
		url: link,
		dataType: 'html',
		cache: true,
		success: function success(data) {
			s.ajaxRequest = false;
			if (typeof data === 'object') {
				data = data.responseText;
				log('GOT CACHED', 'OK');
			}
			let parser = new DOMParser(),
				doc = parser.parseFromString(data, 'text/html');
			const classList = doc.body.classList,
				header = doc.querySelector('header'),
				main = doc.querySelector('main');
			// alert(main);
			appendPage(header, main, classList);
			parser = doc = null;
		},
		error: function error(_error) {
			log('GET PAGED FAILED', 'WARN')(_error);
			s.ajaxRequest = false;
		}
	});
}
export default getPage;