import util from './util';
import addCacheSupport from './addCacheSupport';
import getPageId from './getPageId';
import getPageName from './getPageName';
import addTrasnformIcon from './addTrasnformIcon';
import toggleNav from './toggleNav';
import toggleNavBtn from './toggleNavBtn';
import changeBodyClasses from './changeBodyClasses';
import removeContent from './removeContent';
import hideLoader from './hideLoader';
import toggleLoader from './toggleLoader';
import getPage from './getPage';
import getFormData from './getFormData';
import validateForm from './validateForm';
import hideForm from './hideForm';
import sendForm from './sendForm';
import getSingle from './getSingle';
import centerBox from './centerBox';
import Matcher from './Matcher';
import search from './search';
const methods = {
	search,
	Matcher,
	centerBox,
	getSingle,
	sendForm,
	hideForm,
	getFormData,
	validateForm,
	getPage,
	toggleLoader,
	hideLoader,
	toggleNavBtn,
	getPageId,
	getPageName,
	removeContent,
	addCacheSupport,
	addTrasnformIcon,
	toggleNav,
	changeBodyClasses,
	util
};
export {
	methods as default,
	Matcher,
	search,
	centerBox,
	getSingle,
	sendForm,
	hideForm,
	getFormData,
	validateForm,
	getPage,
	toggleLoader,
	hideLoader,
	toggleNavBtn,
	getPageId,
	getPageName,
	removeContent,
	addCacheSupport,
	addTrasnformIcon,
	toggleNav,
	changeBodyClasses,
	util
}