import $ from 'jquery';
import cache from '../cache';
import cacheData from '../cacheData';
const addCacheSupport = function() {
	$
		.ajaxPrefilter(function(options, originalOptions, jqXHR) {
			// options.cache = false;
			if (options.cache) {
				var complete = originalOptions.complete || originalOptions.success || $.noop,
					url = originalOptions.url;
				options.cache = false;
				options.beforeSend = function() {
					var c = cache.exist(url);
					if (cache.exist(url)) {
						complete(cache.get(url));
						return false;
					}
					return true;
				};
				options.complete = function(data, textStatus) {
					cache.set(url, data, complete);
				};
			}
		});
}
export default addCacheSupport