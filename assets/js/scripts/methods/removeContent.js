import $ from 'jquery';
import elms from '../elms.js';
const removeContent = function() {
	// var header = elms
	// 	.body
	// 	.find('header');
	// $(header).remove();
	// var main = elms
	// 	.body
	// 	.find('main');
	// $(main).remove();
	var header = elms
		.body
		.find('header');
	$(header).empty();
	var main = elms
		.body
		.find('main');
	$(main).empty();
}
export default removeContent;
