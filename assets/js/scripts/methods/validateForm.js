import $ from 'jquery';
const validateForm = function(form) {
	$(form).validate({
		errorPlacement: function errorPlacement(error, element) {
			console.log(error, element);
			return true;
		},
		debug: true,
		rules: {
			fname: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			tel: {
				required: true,
				digits: true,
				minlength: 8
			}
		}
	});
}
export default validateForm;
