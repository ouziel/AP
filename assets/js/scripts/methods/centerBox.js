import $ from 'jquery';
import {isMobile} from './util';
function centerBox() {
	// target,el ?
	// wait for css to change header VH unit
	setTimeout(() => {
		const padding = 15;
		const header = $('header');
		const box = $('.home_header_text');
		header.width = header.width();
		header.height = header.height();
		box.width = box.width();
		box.height = box.height();
		box.pos_x = (header.width - (box.width + padding * 2)) / 2;
		box.pos_y = (header.height - (box.height + padding * 2)) / 2;
		// if (!isMobile() && box.pos_y < 200) {
		// 	box.pos_y = 25 + '%';
		// }
		// $(box).css({top: box.pos_y, left: box.pos_x});
	}, 500);
}
export default centerBox;