import util, {log, isMobile} from './util';
import settings from '../settings';
const checkIfMobile = function() {
	let bool = isMobile();
	log('IS MOBILE ?', 'bool')(bool);
	settings.isMobile = bool;
}
export default checkIfMobile;
