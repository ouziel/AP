import $ from 'jquery';
import elms from './elms';
import {log} from './methods/util';
import methods, {sendForm, getSingle, toggleNav, getPage} from './methods';
const bindEvents = function() {
	var self = this;
	$(document).on('click', '.site-logo', toggleNav.bind(this));
	$(document).on('click', elms.form_btn, sendForm.bind(this));
	$(document).on('click', elms.nav_toggle_btn, toggleNav.bind(this));
	$.each(elms.navlinks, function(i, el) {
		$(el)
			.on('click', function(e) {
				getPage(e);
			});
	});
	// $(document).on('click', '.element-item', function(e) {
	// 	e.stopPropagation();
	// 	getSingle(this);
	// });
	$(document).on('click', '.single-link', function(e) {
		e.preventDefault();
	})
	log('EVENTS BINDED', 'OK');
}
export default bindEvents;
