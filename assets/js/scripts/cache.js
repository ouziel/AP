import $ from 'jquery';
import cacheData from './cacheData';
const cache = {
	exist: function exist(url) {
		let exsits = cacheData.hasOwnProperty(url) && cacheData[url] !== null;
		return exsits;
	},
	remove: function remove(url) {
		delete cacheData[url];
	},
	get: function get(url) {
		return cacheData[url];
	},
	set: function set(url, cachedData, callback) {
		this.remove(url);
		cacheData[url] = cachedData;
		if ($.isFunction(callback)) 
			callback(cachedData);
		}
	}
export default cache;