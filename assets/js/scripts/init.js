// import bindEvents from './bindEvents';
import addTrasnformIcon from './methods/addTrasnformIcon.js';
import addCacheSupport from './methods/addCacheSupport.js';
import checkIfMobile from './methods/checkIfMobile.js';
import initPageTasks from './routes/initPageTasks.js';
import bindEvents from './bindEvents.js';
import util, {log} from './methods/util.js';
const init = function() {
	log('APP INIT');
	bindEvents();
	addTrasnformIcon();
	addCacheSupport();
	checkIfMobile();
	initPageTasks();
}
export default init;