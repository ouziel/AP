import {log, isMobile} from '../methods/util';
import $ from 'jquery';
import routes from './routes';
import executeTasks from './executeTasks';
const initPageTasks = function(id) {
	// id ?
	executeTasks(routes);
};
export default initPageTasks;
