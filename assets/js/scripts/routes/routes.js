import expertise from './pages/expertise';
import home from './pages/home';
import projects from './pages/projects';
import about from './pages/about';
import single from './pages/single';
import contact from './pages/contact';
const routes = {
	expertise,
	home,
	projects,
	about,
	contact,
	single
};
export default routes;