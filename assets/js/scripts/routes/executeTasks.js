import {hideLoader} from '../methods';
import routes from './routes';
/**
 * run a callback after entering a page
  * @param  [array] routes [contains routes name as keys, callback as value]
 */
const executeTasks = function(routes) {
	let taskAlreadyRan = false;
	const classes = Array.from(document.body.classList),
		pageNames = Object.keys(routes);
	pageNames.forEach(function(r) {
		let cls = 'page-template-' + r,
			archive = 'post-type-archive-' + r,
			single = r;
		$.map(classes, function(x) {
			console.log(x);
			if ((x === cls || x === archive || x === single) && !taskAlreadyRan) {
				const task = routes[r];
				setTimeout(function() {
					hideLoader();
					task();
					taskAlreadyRan = true;
				}, 0)
			}
		});
	});
}
export default executeTasks;