import $ from 'jquery';
import Flickity from 'flickity';
import {getPage} from '../../../methods';
import {isMobile} from '../../../methods/util';
import imagesLoaded from 'flickity-imagesloaded';

function singlePageTasks() {
		$('.projects_archive_btns').on('click', getPage);
		var single_gallery = $('#single_gallery');

if (single_gallery.hasClass('flickity-enabled')) {
	glry.resize();
}
else { 
		const glry = new Flickity('#single_gallery', {
			setGallerySize: false,
			pageDots: false,
			contain: true,
			rightToLeft: true
		});
	}

};
export default singlePageTasks;
