import $ from 'jquery';
import state from '../../../state';
import Flickity from 'flickity';
import {smoothScroll, isMobile} from '../../../methods/util';
// TODO: navigation for categories, tags, search and pages
function asyncRemoveAttrs() {
	return new Promise((res, rej) => {
		let items = $('.p_item');
		let removed = $.each(items, (i, item) => {
			$(item).removeAttr('data-zoom', '');
		});
		if (removed) {
			res();
		}
	});
}
function getTags() {}
function closeSingleBar() {
	const stage = $('.single_project');
	asyncRemoveAttrs().then(() => {
		$(stage).removeClass('single_on');
		emptySinglContent(stage);
		state.projects.singleOpen = false;
	});
}
function handleReadMoreState() {
	let el = document.querySelector('#single_info');
	// btn
	// $('#read_more').fadeIn();
	//
	// text truncate
	//
	console.log(el.scrollHeight);
	if (el.scrollHeight > 175) {
		$(el).addClass('truncate-text');
	}
}
function emptySinglContent(stage) {
	$(stage)
		.find('#single_title')
		.hide()
		.empty();
	$(stage)
		.find('#single_info')
		.hide()
		.empty()
	$(stage)
		.find('#single_gallery')
		.hide()
		.empty();
}
function showflickityGallery(images, id) {
	if (!images) {
		return false;
	}
	const {lastGallery} = state.projects;
	const gallery = $(`<div id=gallery-${id} class="single_project_gallery"></div>`);
	images.forEach(url => {
		let item = $('<div class="single_project_img_box"></div>');
		let img = $(`<img src="${url}"/>`);
		item.append(img);
		gallery.append(item);
	});
	$('#single_gallery')
		.empty()
		.append(gallery);
	if (lastGallery) {
		lastGallery.destroy();
	}
	const glry = new Flickity('#gallery-' + id, {
		setGallerySize: false,
		pageDots: false,
		contain: true
	});
	state.projects.lastGallery = glry;
	return glry;
}
function openSingleBar(el) {
	if (!el)
		return false;
	state.projects.singleOpen = true;
	const urls = $(el).data('gallery');
	let images = urls.split(' ');
	images.pop();
	const stage = $('.single_project');
	const info = $(el).data('info');
	const title = $(el).data('title');
	const id = $(el).data('id');
	const link = $(el)
		.find('a')
		.attr('href');
	const catName = $(el).data('categoryname');
	console.log(catName);
	const tags = $(el)
		.find('.tags')
		.clone()
		.children();
	const glry = showflickityGallery(images, id);
	$('#single_title')
		.text(title)
		.fadeIn();
	$('#single_info')
		.text(info)
		.fadeIn()
		.removeClass('truncate-text');
	$('#single_tags')
		.empty()
		.append(tags);
	$('#single_link').attr('href', link);
	$('#single_cat').text(catName);
	$(stage).addClass('single_on');
	glry.resize();
	handleReadMoreState();
	window.location.href = '#projects_form';
	if (!isMobile() && $('body').scrollTop() > 500) {}
}
// $(document).on('click', function(e) {
// 	if (state.projects.singleOpen && !$(e.target).hasClass('p_item')) {
// 		closeSingleBar();
// 	}
// });
$(document).on('click', '.p_close_btn', closeSingleBar);
//
const getSingle = function(e) {
	e.preventDefault();
	const el = e.target;
	const zoomed = $(el).attr('data-zoom');
	if (zoomed) {
		closeSingleBar();
	} else {
		asyncRemoveAttrs().then(() => {
			$(el).attr('data-zoom', 'true');
			openSingleBar(el);
		});
	}
};
export default getSingle;