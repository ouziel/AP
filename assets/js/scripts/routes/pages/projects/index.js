//TODO:
// - grid looks better with 25% width items than 33%
//
// - figure out how to link from homepage/expertise page to a single project
// as in, navigate to projects page and open the queried single object
// consider mobile doesn't show single project as well..
//
// options? query params in url / use inner state object
// -  perhaps change single page loading from destroying/constructing new flickity
// to just changing to data inside a current flickity and reinit it or smth
//
import $ from 'jquery';
import {isMobile, log, smoothScroll} from '../../../methods/util';
import Flickity from 'flickity';
import Isotope from 'isotope-layout';
import {Matcher, getPage, search} from '../../../methods';
import filter from './filter';
import getSingle from './getSingle';
import projectsMQL from './projectsMQL';
import elms from '../../../elms';
function addMediaQueries() {
	let gridMediaQueries = new Matcher({projectsMQL});
	$(window).on('resize', function() {
		gridMediaQueries.resize();
	});
	return gridMediaQueries;
}
function adjustSearchPages(div) {
	let pages = parseInt(div.find('.p_grid').attr('data-page'));
	console.log(pages);
	let filtersBox = $('#pages_filters');
	filtersBox.empty();
	for (let i = 0; i < pages; i++) {
		let opt = $('<option/>');
		opt.attr({
			value: i + 1
		});
		opt.text(i + 1);
		filtersBox.append(opt);
	}
	if (pages === 0) {
		let opt = $('<option/>');
		opt.attr({value: '1', selected: true});
		opt.text('1');
		filtersBox.append(opt);
	}
}
function appendResults(x, gridMediaQueries) {
	// make div to extract html
	let ppp = 9;
	let div = $('<div></div>').html(x.html);
	let grid = div
		.find('.p_grid')
		.children();
	if (grid.length) {
		$('.no_posts').hide();
		$('.p_grid')
			.empty()
			.html(grid);
	} else {
		$('.p_grid').empty();
		$('.no_posts').fadeIn();
	}
	let f = grid[0];
	adjustSearchPages(div);
	gridMediaQueries.resize();
}
function handleFiltersForm(form, gridMediaQueries, e) {
	if (e) {
		e.preventDefault();
	}
	let d = $(form).serialize();
	search(d, function(x) {
		appendResults(x, gridMediaQueries)
	});
}
function addProjectEvents(gridMediaQueries) {
	const form = $('#projects_form');
	form.on('submit', function(e) {
		handleFiltersForm(form, gridMediaQueries, e);
	});
	// $(document).on('click', '.p_filter_btn', filter);
	$(document).off('click', '.p_item');
	$(document).on('click', '.p_item', getSingle);
	$('#pages_filters').change(function(e) {
		$(form).trigger('submit');
	});
	$('#categories_filters').change(function(e) {
		$(form).trigger('submit');
	});
	$('#tags_filters').change(function(e) {
		$(form).trigger('submit');
	});
	$(document).on('click', '#single_link', function(e) {});
	$('#single_link').on('click', function(e) {
		getPage(e);
	});
	// $('#mobile_select_category').on('click', function(e) {
	// 	let a = $('#categories_filters');
	// 	a.trigger();
	// 	a.foucs();
	// 	a.select();
	// });
}
const projectsTasks = function() {
	// addTagsFlickity();
	let gridMediaQueries = addMediaQueries();
	addProjectEvents(gridMediaQueries);
	$('.p_grid').animate({
		opacity: 1
	}, 250);
};
export default projectsTasks;