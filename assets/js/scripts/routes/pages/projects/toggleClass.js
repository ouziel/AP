import $ from 'jquery';
function toggleClass(name) {
	const item = $('.p_item');
	let cls,
		cls1 = 'item-one',
		cls2 = 'item-two',
		cls3 = 'item-three',
		cls_last = 'item-last';
	if (name === 'mobile') {
		cls = cls1;
	} else if (name === 'tablet') {
		cls = cls2;
	} else if (name === 'desktop') {
		cls = cls3;
	}
	let classes = `${cls1} ${cls2} ${cls3} ${cls_last}`;
	let p = new Promise((res) => {
		let t = $(item).removeClass(classes);
		if (t) {
			res();
		}
	})
	p.then(() => item.addClass(cls))
}
export default toggleClass;
