import toggleClass from './toggleClass';
const projectsMQL = {
	mobile: {
		portarait: {
			cb: toggleClass.bind('one'),
			min: 320,
			max: 420
		},
		landscape: {
			cb: toggleClass.bind('two'),
			min: 320,
			max: 420
		}
	},
	tablet: {
		portarait: {
			cb: toggleClass.bind('two'),
			min: 420,
			max: 920
		},
		landscape: {
			cb: toggleClass.bind('three'),
			min: 920,
			max: 1200
		}
	},
	desktop: {
		cb: toggleClass.bind('three'),
		min: 1200
	},
	screen: {
		cb: toggleClass.bind('three'),
		min: 1600
	}
}
export default projectsMQL;
