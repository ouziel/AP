// import $ from 'jquery';
// import d3 from './d3';
// function makeLayout(data, el) {
// 	console.log(d3)
// 	/// projects cluster
// 	var margin = {
// 			top: 140,
// 			right: 10,
// 			bottom: 140,
// 			left: 10
// 		},
// 		w = $('#main').width() * 0.35,
// 		ww = (w > 350)
// 			? 350
// 			: w,
// 		width = ww - margin.left - margin.right,
// 		potentional_height = data.children.length * 70;
// 	var height;
// 	if (potentional_height < 450) {
// 		height = 450 - (margin.top + margin.bottom);
// 	} else {
// 		height = potentional_height - (margin.top + margin.bottom);
// 	}
// 	console.log(w * 0.35);
// 	var cluster = d3
// 		.layout
// 		.cluster()
// 		.size([
// 			height, width - 160
// 		])
// 	// RTL
// 	var diagonal = d3
// 		.svg
// 		.diagonal()
// 		.projection(function(d) {
// 			return [
// 				width - d.y,
// 				height - d.x
// 			];
// 		});
// 	// TOP TO BOTTOM
// 	//var diagonal = d3.svg.diagonal()
// 	//   .projection(function(d) { return [d.x, d.y]; });
// 	const svg = d3
// 		.select('#tree')
// 		.append('div')
// 		.attr('class', 'tree_item')
// 		.append("svg")
// 		.attr("width", width + margin.left + margin.right)
// 		.attr("height", height + margin.top + margin.bottom)
// 		.append("g")
// 		.attr("transform", "translate(" + -(margin.left * 6) + "," + margin.top + ")");
// 	var nodes = cluster.nodes(data);
// 	var link = svg
// 		.selectAll(".link")
// 		.data(cluster.links(nodes))
// 		.enter()
// 		.append("path")
// 		.attr("class", "link")
// 		.attr("d", elbow);
// 	var node = svg
// 		.selectAll(".node")
// 		.data(nodes)
// 		.enter()
// 		.append("g")
// 		.attr("class", "node")
// 		// RTL
// 		.attr("transform", function(d) {
// 			return "translate(" + (-d.y + width) + "," + (-d.x + height) + ")";
// 		})
// 		// TOP TO BOTTOM
// 		//   .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
// 		.on('click', showInfo);
// 	node
// 		.append("circle")
// 		.attr("r", 4.5);
// 	node
// 		.append("text")
// 		.attr("dx", function(d) {
// 			return d.children
// 				? 12
// 				: -12;
// 		})
// 		.attr("dy", d => d.children
// 			? 5
// 			: 5)
// 		.attr("text-anchor", function(d) {
// 			return d.children
// 				? "start"
// 				: "end";
// 		})
// 		.attr('font-size', d => d.children
// 			? '16px'
// 			: '15px')
// 		.attr('font-family', 'Open Sans Hebrew')
// 		.style('font-weight', 'normal')
// 		.text(function(d) {
// 			return d.name;
// 		});
// 	// LTR
// 	//function elbow(d, i) {
// 	//  return "M" + d.source.y + "," + d.source.x
// 	//      + "V" + d.target.x + "H" + d.target.y;
// 	//}
// 	// RTL
// 	function elbow(d, i) {
// 		return "M" + (width - d.source.y) + "," + (height - d.source.x) + "h" + (-25) + "V" + (height - d.target.x) + "H" + (width - d.target.y)
// 		//+ "V" + (height-d.target.x) + "H" + 1.5*(width-d.target.y);
// 	}
// 	//TOP TO BOTTOM
// 	//function elbow(d, i) {
// 	//  return "M" + d.source.x + "," + d.source.y
// 	//      + "V" + d.target.y + "H" + d.target.x;
// 	//}
// 	var article_img = $('#article_img'),
// 		article_title = $('#article_title'),
// 		article_content = $('#article_content');
// 	function showInfo(info) {
// 		let group = d3.select(this);
// 		svg
// 			.selectAll('circle')
// 			.attr('r', 4.5)
// 			.style('fill', '#0080ff');
// 		svg
// 			.selectAll('text')
// 			.style('font-weight', 'normal');
// 		group
// 			.select('text')
// 			.transition()
// 			.duration(500)
// 			.style('font-weight', 'bold');
// 		group
// 			.select('circle')
// 			.transition()
// 			.duration(500)
// 			.attr('r', 6.5)
// 			.style('fill', '#0037FF');
// 		console.log(this);
// 		var img = info.img,
// 			title = info.name,
// 			content = info.content;
// 		$(article_content).text(content);
// 		$(article_img).attr('src', img);
// 		$(article_title).text(title);
// 	}
// }
// export default makeLayout;