import $ from 'jquery';
function handleTreeClick(el) {
	let info = $(el).data('info'),
		tree = $(el).data('treeid')
	changeInfoText(info);
	changeTreeFocus(tree);
	changeTreeDot(el);
}
function changeTreeDot(el) {
	let dot = $(el).find('span')[0];
	let dots = $('.tdot');
	$(dots).removeClass('selected-branch');
	$(dot).addClass('selected-branch');
}
function changeTreeFocus(tree) {
	let icon_boss = $('#icon_boss'),
		icon_info = $('#icon_info');
	if (parseFloat(tree) === 1) {
		icon_info.addClass('icon_on');
		icon_boss.removeClass('icon_on');
	} else {
		icon_boss.addClass('icon_on');
		icon_info.removeClass('icon_on');
	}
}
function changeInfoText(text) {
	$('#info_p')
		.hide()
		.text(text)
		.fadeIn('fast');
}
export default handleTreeClick;