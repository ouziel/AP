import $ from 'jquery';
import Flickity from 'flickity';
import {isMobile} from '../../../methods/util';
import handleTreeClick from './handleTreeClick';
function switchSlider({type, flky_members, flky_profile}) {
	console.log('a');
	let members = $('#members_gallery'),
		profiles = $('#profile_gallery'),
		icons = $('.icon-info'),
		icon_profile = $('#icon-info'),
		icon_members = $('#icon-boss');
	if (type === 'profile') {
		members.hide();
		icons.removeClass('icon-on');
		icon_profile.addClass('icon-on');
		profiles.fadeIn();
		flky_profile.resize();
	} else {
		profiles.hide();
		icons.removeClass('icon-on');
		icon_members.addClass('icon-on');
		members.fadeIn();
		flky_members.resize();
	}
}
const aboutTasks = function() {
	if (isMobile()) {
		var flky_members = initGallery('.members_gallery');
		var flky_profile = initGallery('.profile_gallery');

		$('#about_info_slider_btn').on('click', function(e) {

			switchSlider({type: 'profile', flky_members, flky_profile});
		});
		$('#about_company_slider_btn').on('click', function(e) {
			switchSlider({type: 'Management', flky_members, flky_profile});
		});
	} else {
		$(document)
			.on('click', '.branch', function(e) {
				let self = this;
				handleTreeClick(this);
				// handleTreeClick.bind(this); // ??
			});
	}
	console.log('ABOUT PAGE');
}
export default aboutTasks;

function initGallery(el) {
	var elm = $(el), flky;
	if (!elm.hasClass('flickity-enabled')) {
		flky = new Flickity(el, {contain: true});

	}
	return flky;
}
