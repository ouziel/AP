import $ from 'jquery';
import {centerBox, getPage} from '../../methods';
import flickity from 'flickity/dist/flickity.pkgd.min.js';
import s from '../../settings';
const homeTasks = function() {
	if (!$('.home-gallery').hasClass('flickity-enabled')) {
		const flky = new flickity('.home-gallery', {
			contain: true,
			pageDots: false,
			setGallerySize: false,
			autoPlay: 3000
		});
		flky.resize();
	}
	$('.home-gallery').fadeIn();
	$('.project').off('click');
	$('.project').on('click', function(e) {
		e.preventDefault();
		let href = $(this)
			.find('a')
			.attr('href');
		getPage(e, href);
	});
}
export default homeTasks;