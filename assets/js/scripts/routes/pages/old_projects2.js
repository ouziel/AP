import $ from 'jquery';
import {isMobile, log} from '../../methods/util';
import Flickity from 'flickity';
import Isotope from 'isotope-layout';
import {Matcher} from '../../methods';
function mobileGallery() {
	const galleries = $('.projects_gallery');
	$.each(galleries, function(index, glry) {
		var flkty = new Flickity(glry, {
			touchVerticalScroll: false,
			contain: true,
			pageDots: false,
			setGallerySize: false
		});
		var glryID = $(this).attr('id');
		var progressBar = $('#progress-' + glryID);
		// duck punch
		var _positionSlider = flkty.positionSlider;
		flkty.positionSlider = function() {
			_positionSlider.apply(flkty, arguments);
			var firstCell = flkty.cells[0];
			var cellsWidth = flkty
				.getLastCell()
				.target - firstCell.target;
			var progress = (-flkty.x + -firstCell.target) / cellsWidth;
			progress = Math.max(0, Math.min(1, progress));
			var val = progress * 100 + '%';
			$(progressBar).css('width', val);
		};
	});
	// bind toolbar events
	const search_box = $('#projects_search_box'),
		select_box = $('#projects_select_box');
	// icons click event
	// $('#projects_search_icon').on('click', function(e) {
	// 	e.preventDefault();
	// 	$(select_box).hide();
	// 	$(search_box).show();
	// });
	$('#projects_filters_icon').on('click', function(e) {
		e.preventDefault();
		// $(search_box).hide();
		$(select_box).show();
	});
	// select on change event
	var select = $(select_box).find('select');
	$(select).on('change', function(e) {
		alert('a');
		$(select_box).trigger('submit');
	});
	$(select_box).on('submit', function(e) {
		e.preventDefault();
		var data = $(this).serializeArray();
		var glry = data[0].value;
		$('.projects_gallery').hide();
		$('.progress-bar').hide();
		// $('#' + glry).fadeIn('fast');
		$('#' + glry).slideDown('fast');
		$('#progress-' + glry).fadeIn('fast');
	});
	// $(search_box).on('submit', (e) => {
	// 	e.preventDefault();
	// 	var data = JSON.stringify($(this).serializeArray());
	// 	// alert(data);
	// 	// alert($(e.target).serialize());
	// });
}
const projectsTasks = function() {
	if (isMobile()) {
		mobileGallery();
		// desktopGallery();
	} else {
		desktopGallery();
		// mobileGallery();
		// $(window).on('resize', desktopGallery());
	}
};
export default projectsTasks;
