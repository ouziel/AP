import $ from 'jquery';
import Flickity from 'flickity';
import {isMobile, log} from '../../methods/util';
import {TweenMax, TimelineMax} from 'gsap';
// import MorphSVGPlugin from 'gsap/src/uncompressed/plugins/MorphSVGPlugin.js';
function initGallery(el,options) {
	var elm = $(el), flky;
	if (!elm.hasClass('flickity-enabled')) {
		flky = new Flickity(el,options);
	}
	return flky;
}

const expertiseTasks = function() {
	if (isMobile()) {
    const glryOptions = {
      touchVerticalScroll: false,
      contain: true
    }
    let glry = initGallery('.expertise-gallery', glryOptions);
	} else {
		// ?
		// MorphSVGPlugin.convertToPath('circle, rect');
		$('.s_item').off('click');
		$('.s_item').on('click', handleClick);
		function handleClick(e) {
			e.preventDefault();
			let zoomed = $(this).attr('data-zoom');
			if (zoomed) {
				$(this).removeAttr('data-zoom', '');
				hideInfo(this)
			} else {
				$(this).attr('data-zoom', 'true');
				showInfo(this)
			}
		}
	}
}
function showInfo(t) {
	let tl = new TimelineMax({
		onComplete: () => console.log('done!')
	});
	let clip = $(t).find('defs clipPath rect'),
		rect = $(t).find('svg path')[0],
		s_item_line = $(t).find('svg .s_item_line'),
		s_item_title = $(t).find('svg .s_item_title'),
		info = $(t).find('.s_p_info');
	let width = $(t).width();
	tl.to(s_item_title, 0.2, {
		attr: {
			x: 0,
			'text-anchor': 'end'
		},
		css: {
			opacity: 0
		},
		ease: Sine.easeOut
	}).to(clip, 0.5, {
		attr: {
			width: '10px'
		},
		ease: Sine.easeOut
	}, "-=0.2").to(info, 0.5, {
		x: 0,
		opacity: 1,
		ease: Sine.easeOut
	}, "-=0.5")
}
function hideInfo(t) {
	let tl = new TimelineMax({
		onComplete: () => console.log('done!')
	});
	let clip = $(t).find('defs clipPath rect'),
		rect = $(t).find('svg path')[0],
		s_item_line = $(t).find('svg .s_item_line'),
		s_item_title = $(t).find('svg .s_item_title'),
		info = $(t).find('.s_p_info');
	tl
		.to(info, 0.5, {
		x: 2000,
		opacity: 0,
		ease: Sine.easeOut
	})
		.to(clip, 0.5, {
			attr: {
				width: "700px"
			},
			ease: Sine.easeOut
		}, "-=0.5")
	tl.to(s_item_title, 0.2, {
		attr: {
			x: '50%',
			'text-anchor': 'middle'
		},
		css: {
			opacity: 1
		},
		ease: Sine.easeOut
	}, "-=0.1")
}
export default expertiseTasks;;
