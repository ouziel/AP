import $ from 'jquery';
const elms = {
	body: $('body'),
	mainNav: '.main-navigation',
	nav_toggle_btn: '#nav-toggle-btn',
	toggleBtn: '.tcon-menu--xbutterfly',
	menu_header: $('#menu-header-mobile'),
	form_btn: '#contact_btn',
	form: $('#contact_form'),
	formid: '#contact_form',
	msg: $('#form_complete_msg'),
	contact_msg: $('#contact_msg'),
	navlinks: $('.get_page_ajax'),
	loader: $('#loader'),
	gallery: '.gallery',
	circlesContainer: '#circles_container'
};
export default elms;
