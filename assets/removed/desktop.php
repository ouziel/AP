<?php
function projectsDesktopLayout($terms, $query)
{
    if ($query->have_posts()) {

        // filter  category buttons
        //////////////////////
        echo '<div class="button-group filters-button-group">';
        echo '<div class="button is-checked" data-filter="*">' . pll__('Show All') . '</div>';
        foreach ($terms as $term) {
            $name   = $term->name;
            $id     = $term->term_taxonomy_id;
            $filter = '.cat-' . $id;

            echo '<div class="button" data-filter=' . $filter . '>' . $name . '</div>';
        }
        echo '</div>';
        echo '</section>';

        /// single

        echo '<div class="single_project">
              <div class="inner"></div>
              </div><section>';

        /// grid;
        /////////////////
        echo '<div class="grid">';
        echo '<div class="grid-sizer"></div>';
        $i = 0;
        while ($query->have_posts()) {
            $query->the_post();
            $id       = get_the_ID();
            $title    = get_the_title();
            $category = get_the_category($id);
            $url      = get_permalink();

            $cat_id  = $category[0]->cat_ID;
            $filter  = 'cat-' . $cat_id;
            $gallery = get_field('project_gallery', $id);
            $info    = strip_tags(get_field('project_desc',$id));

            $classes = array();
            array_push($classes, 'element-item');
            array_push($classes, $filter);

            if ($gallery) {
                $img = $gallery[0]['url'];
            }
            else {
                $img = '';
              }

            echo '<div class="' . join(' ', $classes) . '" data-category="' . $filter . '" data-title="'.$title.'" data-img="'.$img .'" data-info="'.$info .'">';

            echo '<a href="'. $url .'" class="single-link"><h4>' . $title . '</h4></a>';
            if (isset($img)) {
                echo '<img src="' . $img . '" />';
            }
            $i++;
            echo '</div>';

        }
        echo '</div>';
    } else {
        // no posts found
    }
    /* Restore original Post Data */
    wp_reset_postdata();

}


 ?>
