<?php
function projectsMobileLayout($terms, $query)
{
    $posts = new stdClass();

    if ($query->have_posts()) {


        echo '<div class="projects_toolbar">';

        echo '<div class="icons">';
        echo '<button class="search_icon" id="projects_search_icon">
    </button>';
        echo '<button class="filters_icon" id="projects_filters_icon">
    </button>';
        echo '<h5 class="filters_text">סינון:</h5>';
        echo '</div>';

        echo '<div class="fields" id="projects_toolbar">';
        echo '<form class="projects_search_box" id="projects_search_box">
          <input name="search_input" type="text"/ ></form>';
        echo '<form class="projects_select_box" id="projects_select_box">';
        echo '<select name="filter_input">';
        echo '<option>' . pll__('Show All') . '</option>';
        foreach ($terms as $term) {
            $name   = $term->name;
            $id     = $term->term_taxonomy_id;
            $filter = 'cat-' . $id;

            echo '<option name="'. $filter .'" class="button" value=' . $filter . '>' . $name . '</option>';
        }
        echo '</select></form>';

        echo '</div>';

        echo '</div>';

        while ($query->have_posts()) {

            $query->the_post();
            $id       = get_the_ID();
            $title    = get_the_title();
            $category = get_the_category($id);
            $url      = get_permalink();

            $cat_id  = $category[0]->cat_ID;
            $filter  = 'cat-' . $cat_id;
            $info    = get_field('project_desc',$id);
            $gallery = get_field('project_gallery', $id);

            $data = array(
                'title' => $title,
                'id' => $id,
                'cat' => $cat_id,
                'gallery' => $gallery,
                'info' => $info,
            );
            if (!property_exists($posts, $cat_id)) {
                $posts->$cat_id = array();
            }
            array_push($posts->$cat_id, $data);

        }

        foreach ($posts as $key => $values) {


            $filter = 'cat-' . $key;
            echo '<div class="projects_gallery" id="' . $filter . '">';

            foreach ($values as $item) {

                $img = $gallery[0]['url'];
                echo '<div class="project">';
                if ($img) {
                  echo '<img src="'.$img.'"/>';
                }
                echo '<h4 class="project_title">'.$item['title'].'</h4>';
                echo '</div>';
                // echo '<a href="'. $url .'" class="get_page_ajax"><h4>title:' . $item['title'] . '</h4></a>';

            }

            echo '</div>';
            echo '<div class="progress-bar" id="progress-'. $filter .'"></div>';
        }

    }


}
 ?>