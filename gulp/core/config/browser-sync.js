// utils
const deepMerge = require('../utils/deepMerge');
// config
const overrides = require('../../config/browser-sync');
/**
 * BrowserSync
 * configuration
 * object
 *
 */
module.exports = deepMerge({
	logSnippet: false,
	ghostMode: false,
	open: false,
	port: 3000,
	notify: false,
	proxy: 'localhost:90/ESNC/AP/WordPress/',
	watchOptions: {
		debounceDelay: 2000
	}
}, overrides);
