/**
 * Override the Default
 * Core Scripts
 * Config
 *
 */
var webpack = require('webpack');
module.exports = {
	options: {
		webpack: {
			defaults: {
				plugins: [new webpack.ProvidePlugin({'gsap': 'gsap', 'd3': 'd3', '$': 'jquery', 'jQuery': 'jquery', 'window.jQuery': 'jquery'})],
				resolve: {
					alias: {
						'eventEmitter/EventEmitter': 'wolfy87-eventemitter/EventEmitter',
						'get-style-property': 'desandro-get-style-property',
						'matches-selector': 'desandro-matches-selector',
						'classie': 'desandro-classie',
						'masonry': 'masonry-layout',
						'isotope': 'isotope-layout'
					}
				}
			}
		}
	}
};