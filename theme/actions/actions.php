<?php

add_action('dynamic_header','getHeader');
add_action('page_title','getPageTitle');


add_action('expertise_mobile_layout','exprtiseMobileLayout',10,1);
add_action('expertise_desktop_layout','exprtiseDesktopLayout',10,1);

add_action('projects_archive_desktop','projectsDesktopLayout',10,2);
add_action('projects_archive_mobile','projectsMobileLayout',10,2);


add_action('projects_archive','projectsLayout',10,2);
add_action('projects_search','projects_grid',10,2);

add_action('single_project','singleProject',10,1);


add_action ('home_projects','getHomeProjects');
add_action ('about_layout','about_page_layout');



add_filter( 'nav_menu_link_attributes', 'add_menu_atts', 10, 3 );
add_filter( 'wp_image_editors', 'change_graphic_lib' );

function change_graphic_lib($array) {
return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}
// disable WP updates
add_filter( 'automatic_updater_disabled', '__return_true' );
 ?>
