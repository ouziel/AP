<?php
/*
 * Template Name: Homepage
 * Description: Homepage Template
 */
?>


<?php
get_header();
?>


<main id="stage" class="container">

  <?php
  do_action('home_projects');
  ?>
</main>


<?php
get_footer();
 ?>
