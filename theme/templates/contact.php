<?php
/*
* Template Name: Contact
* Description: Contact Template
 */
 ?>
 <!--  HEADER -->
 <?php get_header(); ?>

 <?php

      $tel     =     get_field( 'biz_main_tel');
      $email   =     get_field( 'biz_email');
      $address =     get_field( 'biz_address');
      $fax     =     get_field( 'biz_fax');

?>


<!--  MAIN  -->
<main id="stage" class="container shapes">

    <!--  PAGE TITLE  -->
    <?php do_action('page_title','Contact'); ?>

    <!--  FORM  -->
    <?php include(locate_template('templates/parts/form.php')); ?>


    <!--  SOCIAL LINKS  -->
    <?php include(locate_template('templates/parts/links.php')); ?>


</main>

<!--  FOOTER  -->
<?php get_footer(); ?>
