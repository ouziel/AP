<?php
/*
 * Template Name: Expertise
 * Description: Expertise Template
 */
?>


<?php
get_header();
?>


<main id="stage" class="container shapes">

  <!--  PAGE TITLE  -->
  <?php do_action('page_title', 'Expertise'); ?>


  <?php
  global $post;
  $id = $post->ID;

  if (wp_is_mobile()) {

      do_action('expertise_mobile_layout', $id);

  }
  else {

      // do_action('expertise_mobile_layout', $id);
      do_action('expertise_desktop_layout', $id);

    }
  ?>

</main>



<?php
get_footer();
?>
