<section class="form">

    <div id="form_complete_msg" class="complete_msg">

        <div class="contact_form_row">
            <h5 class="section_title">  <span><?php pll_e('Leave a message'); ?></span></h5>
        </div>

        <div class="contact_msg">
            <h5>
                  <span>
                      <?php pll_e('Your message was successfuly saved.'); ?>
                  </span>
                  <span>
                      <?php pll_e("We'll be in touch soon"); ?>
                  </span>
            </h5>
        </div>
    </div>

<form id="contact_form" action="" class="contact_page_form" autocomplete="on">

    <div class="contact_form_row">
        <h5 class="section_title">  <span><?php pll_e('Leave a message'); ?></span></h5>
    </div>

    <div class="contact_form_row">
        <input type="text" name="fname" placeholder="<?php pll_e('Full Name'); ?>" autocomplete="name" required/>
    </div>

    <div class="contact_form_row">
        <input type="email" name="email" placeholder="<?php pll_e('Email Address'); ?>" autocomplete="email" />
    </div>

    <div class="contact_form_row">
        <input type="tel" name="tel" placeholder="<?php pll_e('Phone Number'); ?>" autocomplete="tel" required/>
    </div>

    <div class="contact_form_row">
        <input type="text" name="company" placeholder="<?php pll_e('Company Name'); ?>" autocomplete="org" />
    </div>

    <div class="contact_form_row">
        <textarea  class="contact_textarea" name="msg" rows="4" cols="10" placeholder="<?php pll_e( 'Message'); ?>">
        </textarea>
    </div>

    <div class="contact_form_row">
        <input id="contact_btn" class="form_button" type="submit" value="<?php pll_e('Send'); ?>" />
    </div>

</form>

</section>
