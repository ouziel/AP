
<!--  desktop nav -->
  <div class="main-navigation">
    <?php
       wp_nav_menu(
       array(
         'container_class' => 'desktop_pages_container',
         'theme_location' => 'header',
         'menu_id' => 'menu-header',
         'menu_class' => 'pages-menu-desktop' )
        );
       ?>

       <div class="site-logo">
         <div class="logo-big">
              <div class="logo-big-inner">
                <!-- <svg class="nav_logo"  viewBox="0 0 100 100">
                     <path d="M0.67,63.9 L18.61,3.44 L18.61,56.28 L35.94,56.11 L18.69,117.11 L17.94,63.78 L0.67,63.9 Z"></path>
                </svg> -->
                <svg class="nav_logo"  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                	 viewBox="0 0 54 100" style="enable-background:new 0 0 54 100;" xml:space="preserve">
                   <defs>
                     <filter id="glow">
                  <feGaussianBlur stdDeviation="2.5" result="coloredBlur"/>
                  <feMerge>
                      <feMergeNode in="coloredBlur"/>
                      <feMergeNode in="SourceGraphic"/>
                  </feMerge>
                      </filter>
                   </defs>
                <g filter="url(#glow)">
                  <polygon points="25.4,2.7 20.9,51.6 26.8,51.6 27.7,97.3 32.8,47 26.5,47.1 	"/>
                	<path d="M20.2,22.7c0.1-0.5,0.8-0.7,1.1-0.3v0c0,0,1.1,4.2,1.2,5.6c0.1,1.6,0.1,4.8-0.4,6.3c-0.3,1-1,3.1-1.8,3.6
                		c-0.4,0.2-1.4,0.1-1.7-0.2c-1.5-1.6-0.8-8.1,0.1-8.8c0.2-0.2,1.3,1,1,2.6c-0.1,0.3-0.8,1.1-0.8,1.1l0.1,0.8
                		c0.1,0.7,0.8,1.1,1.4,0.8h0c1.8-1.7,1.1-5.7,1.1-5.7L20.2,22.7z"/>
                	<path d="M33.3,74.9c2.3,0,2.2,0.9,1.2,2c-6.3,6.3-4.6-10.9-1.1-12.5c0.5-0.2,3.1-0.7,2.1,1.3c-0.3,0.5-1,0.4-1.3,0.5
                		c-4.2,1.2-3.5,8.4-1.9,9.2C32.5,75.5,32.5,74.8,33.3,74.9z"/>
                	<rect x="22.2" y="32" width="1.7" height="1.9"/>
                	<rect x="28.7" y="70.8" width="1.9" height="1.9"/>
                </g>
                </svg>
              </div>
         </div>
       </div>
</div>

<!--  mobile nav -->
<div class="mobile_nav">

      <div class="nav-toggle" id="nav-toggle-btn">
        <button type="button" class="tcon tcon-menu--xbutterfly" aria-label="toggle menu">
          <span class="tcon-menu__lines" aria-hidden="true"></span>
          <span class="tcon-visuallyhidden">toggle menu</span>
        </button>
      </div>

      <?php
         wp_nav_menu(
         array(
           'container_class' => 'menu_container',
           'container_id' => 'menu-header-mobile',
           'theme_location' => 'header',
           'menu_class' => 'pages-menu' )
          );
         ?>

         <div class="site-logo">
            <div class="logo_inner">
               <?php echo bloginfo('name'); ?>
            </div>
         </div>
</div>
