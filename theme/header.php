<?php
/**
 * Header file common to all
 * templates
 *
 */
?>
<!doctype html>
<html class="site no-js" <?php language_attributes(); ?>>

<?php
/// HEAD
		get_template_part('templates/parts/head');
?>

<!--  BODY -->
<body <?php body_class(); ?>>

<!--  LOGO LOADER  -->
<?php get_template_part('templates/parts/logoLoader'); ?>

<!-- PAGE ROOT -->
<div id="page" class="site">

	<!--  NAVIGATION -->
	<?php get_template_part('templates/parts/nav'); ?>

<!--  HEADER -->
		<?php do_action('dynamic_header') ?>

<!--  MAIN -->

