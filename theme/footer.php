<?php
/**
 * Footer file common to all
 * templates
 *
 */
?>
<!-- FOOTER -->
<footer>
  <div class="footer_inner">
    <div class="footer_right">
    <?php echo bloginfo('name'); ?> <?php echo date("Y"); ?> &copy;
    </div>
    <div class="footer_left">
      <span>ESNC</span>
    </div>
  </div>
</footer>

<!-- PAGE END -->
</div>
<!--  BG COVER  -->
<div class="bg_cover">
</div>
<!-- LOADER  -->
<div id="loader">
  <span class="tcon-loader--spinner360" aria-label="loading">
    <span class="tcon-visuallyhidden">loading</span>
  </span>
</div>
<?php wp_footer(); ?>


</body>
</html>
