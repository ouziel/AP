<?php
/**
 * Theme Functions &
 * Functionality
 *
 */

  //




/* =========================================
		ACTION HOOKS & FILTERS
   ========================================= */

/**--- Actions ---**/

//// requires



add_action( 'after_setup_theme',  'theme_setup' );

add_action( 'wp_enqueue_scripts', 'theme_styles' );

add_action( 'wp_enqueue_scripts', 'theme_scripts' );

// expose php variables to js. just uncomment line
// below and see function theme_scripts_localize
add_action( 'wp_enqueue_scripts', 'theme_scripts_localize', 20 );

/**--- Filters ---**/


/* =========================================
		HOOKED Functions
   ========================================= */

/**--- Actions ---**/



/**
 * Setup the theme
 *
 * @since 1.0
 */
if ( ! function_exists( 'theme_setup' ) ) {
	function theme_setup() {




		// Let wp know we want to use html5 for content
		add_theme_support( 'html5', array(
			'comment-list',
			'comment-form',
			'search-form',
			'gallery',
			'caption'
		) );


		// Let wp know we want to use post thumbnails
		/*
		add_theme_support( 'post-thumbnails' );
		*/


		// Register navigation menus for theme

		register_nav_menus( array(
			'header' => 'Main Menu',
			'footer'  => 'Footer Menu'
		) );



		// Let wp know we are going to handle styling galleries
		/*
		add_filter( 'use_default_gallery_style', '__return_false' );
		*/


		// Stop WP from printing emoji service on the front
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );


		// Remove toolbar for all users in front end
		show_admin_bar( false );


		// Add Custom Image Sizes
		/*P
		add_image_size( 'ExampleImageSize', 1200, 450, true ); // Example Image Size
		...
		*/



		// Register Autoloaders Loader
		$theme_dir = get_template_directory();
		include "$theme_dir/library/library-loader.php";
		include "$theme_dir/includes/includes-loader.php";
		include "$theme_dir/components/components-loader.php";

    include "$theme_dir/functions/functions.php";
    include "$theme_dir/actions/actions.php";
    include "$theme_dir/config/config.php";

    // require_once('functions/functions.php');
    // require_once('actions/actions.php');
    // require_once('config/config.php');
	}
}


/**
 * Register and/or Enqueue
 * Styles for the theme
 *
 * @since 1.0
 */
if ( ! function_exists( 'theme_styles' ) ) {
	function theme_styles() {
		$theme_dir = get_stylesheet_directory_uri();

		wp_enqueue_style( 'main', "$theme_dir/assets/css/main.css", array(), null, 'all' );
	}
}


/**
 * Register and/or Enqueue
 * Scripts for the theme
 *
 * @since 1.0
 */
if ( ! function_exists( 'theme_scripts' ) ) {
	function theme_scripts() {
		$theme_dir = get_stylesheet_directory_uri();

		wp_register_script( 'validate', 'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js', array('jquery'), false, true );
		wp_enqueue_script( 'validate' );

		// wp_register_script( 'flickity', 'https://cdnjs.cloudflare.com/ajax/libs/flickity/1.2.1/flickity.pkgd.min.js', false, '1.2.1', false );
		// wp_enqueue_script('flickity');

		wp_enqueue_script( 'main', "$theme_dir/assets/js/main.js", array('validate'), null, true );


	}
}


/**
 * Attach variables we want
 * to expose to our JS
 *
 * @since 3.12.0
 */
if ( ! function_exists( 'theme_scripts_localize' ) ) {
	function theme_scripts_localize() {
		$ajax_url_params = array();


		wp_localize_script( 'main', 'wpapp', array(
			'home'  => home_url(),
			'theme' => get_stylesheet_directory_uri(),
			'ajaxurl'  => add_query_arg( $ajax_url_params, admin_url( 'admin-ajax.php' ) )
		) );
	}
}




?>