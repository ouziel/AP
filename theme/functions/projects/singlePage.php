<?php
function singleProject()
{
    // $query->the_post();
    $html      = '';
    $id        = get_the_ID();
    $title     = get_the_title();
    $gallery   = get_field('projects_gallery', $id);
    $info      = strip_tags(get_field('project_desc', $id));
    $post_tags = wp_get_post_tags($id);
    $cats      = get_the_category();
    $tag_names = array();
    // $category = get_the_category($id);
    // $url      = get_permalink();
    // $post_tags = wp_get_post_tags($id);
    foreach ($post_tags as $tag) {
        $the_tag_name = $tag->name;
        array_push($tag_names, $the_tag_name);
    }
    $html .= '<section class="single_project_page">';

    $html .= '<div class="top">';


    $html .= '<button data-link=""><a id="projects_archive_link" href="' . get_post_type_archive_link('projects') . '" class="projects_archive_btns">' . pll__('All Projects') . '</a></button>';
    $html .= '<h4><span>' . $title . '</span></h4>';
    $html .= '</div>';

    $html .= '<div class="bottom">';



    $html .= '<div class="gallery">';
    $html .= '<div class="gallery_box " id="single_gallery" >';
    foreach ($gallery as $img) {
        $html .= '<div class="gallery_cell"><img src="' . $img['url'] . '" /></div>';
    }
    $html .= '</div>';



    if (!wp_is_mobile()) {
        $html .= '<div class="single_category_name">';
        foreach ($cats as $cat) {
            $html .= '<h3><span>' . $cat->cat_name . '</span></h3>';
        }
        $html .= '</div>';
        $html .= '<div class="single_tag_list">';
        foreach ($tag_names as $tag) {
            $html .= '<h4 class="tag"><span>' . $tag . '</span></h4>';
        }
    }
    $html .= '</div>';
    if (!wp_is_mobile()) {
        $html .= '</div>';
    }

    $html .= '<div class="content">';
    if (!wp_is_mobile()) {
        $html .= '<p>' . $info . '</p>';
    }

    if (wp_is_mobile()) {
        $html .= '<div class="single_tag_list_mobile">';
        foreach ($tag_names as $tag) {
            $html .= '<h4 class="tag"><span>' . $tag . '</span></h4>';
        }
        $html .= '</div>';

        $html .= '<p>' . $info . '</p>';
        $html .= '<div class="single_category_name_mobile">';
        foreach ($cats as $cat) {
            $html .= '<h3><span>' . $cat->cat_name . '</span></h3>';
        }
        $html .= '</div>';
        $html .= '<div><button class="mobile_projects_archive_btn"><a id="projects_archive_link" href="' . get_post_type_archive_link('projects') . '" class="projects_archive_btns">'.pll__('All Projects').'</a></button></div>';
    }

    $html .= '</div>';
    $html .= '</section>';

    echo $html;
}
?>
