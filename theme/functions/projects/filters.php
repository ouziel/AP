<?php
function filtersRow($cats,$tags) {
  echo '<form id="projects_form" class="filters_row" id="projects_filters">';


  echo '<div class="row_item">';
  echo '<h3 class="row_label"><span>'.pll__('Categories').'</span></h3>';
  echo '<select name="category_input" id="categories_filters">';
  foreach ($cats as $cat) {
    $filter = 'cat-'.$cat->term_taxonomy_id;
    echo '<option value="'.$filter.'">'.$cat->name.'</option>';
  }
  echo '</select>';

  echo '</div>';

  echo '<div class="row_item">';
  echo '<h3 class="row_label"><span>'.pll__('Tags').'</span></h3>';
  echo '<select name="tag_input" id="tags_filters">';
  echo '<option value="tag-1">'.pll__('Show All').'</option>';
  foreach ($tags as $tag) {
    $filter = 'tag-'.$tag->term_taxonomy_id;
    echo '<option value="'.$filter.'">'.$tag->name.'</option>';
  }
  echo '</select>';

  echo '</div>';

  echo '<div class="row_item">';
  echo '<h3 class="row_label"><span>'.pll__('Search').'</span></h3>';
  echo '<input id="search_filter" type="text" name="search_input"/>';

  // get_search_form ( true );
  echo '</div>';



  echo '<div class="row_item posts_pages">';
  echo '<h3 class="row_label"><span>'.pll__('Pages').'</span></h3>';
  echo '<select name="page_input" id="pages_filters">';
  echo '<option>1</option>';
  echo '</select>';

  echo '<input  type="submit"  value="'.pll__('Submit').'"/>';
  echo '</div>';




  echo '</form>';
}
?>