<?php
function utf8_strrev($str){
    preg_match_all('/./us', $str, $ar);
    return join('',array_reverse($ar[0]));
}
function projects_grid($query,$ajax) {
  $html   = '';

  $html   .= '<section class="p_grid" data-page="'.$query->max_num_pages.'">';
  $i = 0;
  while ($query->have_posts()) {
      $query->the_post();
      $id       = get_the_ID();
      $title    = get_the_title();
      $category = get_the_category($id);
      $link      = get_permalink();
      $post_tags = wp_get_post_tags($id);
      $tags = array();


      $cat_id  = $category[0]->cat_ID;
      $cat_name = $category[0]->name;
      $categories_filter  = 'cat-' . $cat_id;

      // $gallery = get_field('project_gallery', $id);
      $gallery = get_field('projects_gallery', $id);
      $info    = strip_tags(get_field('project_desc',$id));

      $classes = array();
      $urls = array();
      $tag_names = array();

      array_push($classes, 'p_item');
      array_push($classes, $categories_filter);
      foreach ($post_tags as $tag) {
        $the_tag_id = 'tag-'. $tag->term_id;
        $the_tag_name = $tag->name;
        array_push($tag_names, $the_tag_name);
        array_push($tags,$the_tag_id);
        array_push($classes,$the_tag_id);
      }


      if (is_array($gallery)) {
          if (isset($gallery[0]['url'])) {
            $img = $gallery[0]['url'];

          }
          else {
            $img = 'noimg';
          }
          foreach ($gallery as $glry) {
            if (isset($glry['url'])) {
              $url = $glry['url'];
              array_push($urls,$url);
            }
          }
      }
      else {
          $img = 'noimg2';
        }


      $html  .= '<article
                    class="' . join(' ', $classes) . '"
                    data-categoryName="'.$cat_name.'"
                    data-category="' . $categories_filter . '"
                    data-title="'.$title.'"
                    data-img="'.$img .'"
                    data-info="'.$info .'"
                    data-gallery="'.join(' ',$urls).' "
                    data-id="'. $id .'"
                    data-tags="'.join(' ', $tags).'"
                  >';
      $html  .= '<a href="'. $link .'" class="single-link"><h4><span>' . $title . '</span></h4></a>';

      if (isset($img)) {
          $html  .= '<img src="' . $img . '" />';
      }
      $i++;
      $html .= '<div class="tags">';
      foreach ($tag_names as $name) {
      $html .= '<span>'.$name.'</span>';
      }
      $html .= '</div>';
      $html  .= '</article>';

  }
    $html  .= '</section>';


    if ($ajax) {
      $arr = array(
        'html' => $html,
      );
      echo json_encode($arr);
    }
    else {
    echo $html;
  }
}


 ?>