<?php
// Project CPT
function ap_projects_cpt() {

	$labels = array(
		'name'                  => pll__( 'Projects', 'Post Type General Name', 'ap' ),
		'singular_name'         => pll__( 'Project', 'Post Type Singular Name', 'ap' ),
		'menu_name'             => pll__( 'Projects', 'ap' ),
		'name_admin_bar'        => pll__( 'Projects', 'ap' ),
		'archives'              => pll__( 'All Projects', 'ap' ),
		'parent_item_colon'     => pll__( 'Parent Item', 'ap' ),
		'all_items'             => pll__( 'All Items', 'ap' ),
		'add_new_item'          => pll__( 'Add New Item', 'ap' ),
		'add_new'               => pll__( 'Add New', 'ap' ),
		'new_item'              => pll__( 'New Item', 'ap' ),
		'edit_item'             => pll__( 'Edit Item', 'ap' ),
		'update_item'           => pll__( 'Update Item', 'ap' ),
		'view_item'             => pll__( 'View Item', 'ap' ),
		'search_items'          => pll__( 'Search Item', 'ap' ),
		'not_found'             => pll__( 'Not found', 'ap' ),
		'not_found_in_trash'    => pll__( 'Not found in Trash', 'ap' ),
		'featured_image'        => pll__( 'Featured Image', 'ap' ),
		'set_featured_image'    => pll__( 'Set featured image', 'ap' ),
		'remove_featured_image' => pll__( 'Remove featured image', 'ap' ),
		'use_featured_image'    => pll__( 'Use as featured image', 'ap' ),
		'insert_into_item'      => pll__( 'Insert into item', 'ap' ),
		'uploaded_to_this_item' => pll__( 'Uploaded to this item', 'ap' ),
		'items_list'            => pll__( 'Items list', 'ap' ),
		'items_list_navigation' => pll__( 'Items list navigation', 'ap' ),
		'filter_items_list'     => pll__( 'Filter items list', 'ap' ),
	);
	$args = array(
		'label'                 => pll__( 'Project', 'ap' ),
		'description'           => pll__( 'projects', 'ap' ),
		'labels'                => $labels,
		'supports'              => array( ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_rest'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',

	);
	register_post_type( 'Projects', $args );


}
add_action( 'init', 'ap_projects_cpt', 0 );



 ?>
