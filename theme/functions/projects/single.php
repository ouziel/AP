<?php

function single_row() {
  echo '<section class="single_project" id="single_project">';
  echo '<div class="p_close_btn"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjA1OSAzMS4wNTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMxLjA1OSAzMS4wNTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMjRweCIgaGVpZ2h0PSIyNHB4Ij4KPGc+Cgk8Zz4KCQk8cGF0aCBkPSJNMzAuMTcxLDMxLjA1OWMtMC4yMjYsMC0wLjQ1NC0wLjA4Ny0wLjYyNy0wLjI2TDAuMjYsMS41MTVjLTAuMzQ3LTAuMzQ2LTAuMzQ3LTAuOTA4LDAtMS4yNTUgICAgczAuOTA4LTAuMzQ3LDEuMjU1LDBsMjkuMjg0LDI5LjI4NGMwLjM0NywwLjM0NywwLjM0NywwLjkwOCwwLDEuMjU1QzMwLjYyNSwzMC45NzIsMzAuMzk5LDMxLjA1OSwzMC4xNzEsMzEuMDU5eiIgZmlsbD0iIzkxREM1QSIvPgoJPC9nPgoJPGc+CgkJPHBhdGggZD0iTTAuODg4LDMxLjA1OWMtMC4yMjgsMC0wLjQ1NC0wLjA4Ny0wLjYyOC0wLjI2Yy0wLjM0Ny0wLjM0Ny0wLjM0Ny0wLjkwOCwwLTEuMjU1TDI5LjU0NCwwLjI2ICAgIGMwLjM0Ny0wLjM0NywwLjkwOC0wLjM0NywxLjI1NSwwYzAuMzQ3LDAuMzQ3LDAuMzQ3LDAuOTA5LDAsMS4yNTVMMS41MTUsMzAuNzk5QzEuMzQyLDMwLjk3MiwxLjExNSwzMS4wNTksMC44ODgsMzEuMDU5eiIgZmlsbD0iIzkxREM1QSIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" /></div>';
  echo '<div class="single_content">';
  echo '<h4><span id="single_title"></span></h4>';
  echo '<p id="single_info"></p>';
  echo '<div class="extra_row">';
  echo '<button id="read_more_btn"><a id="single_link" href="#" > '.pll__('Project Page').'</a></button>';
  echo '<h3><span id="single_cat"></span></h3>';
  echo '</div>';
  echo '</div>';
  echo '<div class="single_gallery" id="single_gallery"></div>';
  echo '<div id="single_tags" class="single_tags"><div>';

  echo '</section>';
}

?>