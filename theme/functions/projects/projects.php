<?php
require_once('filters.php');
require_once('single.php');
require_once('singlePage.php');
require_once('grid.php');

function projectsLayout($terms, $query)
{
    echo '<div class="p_wrapper">';

    $cats = $terms;
    $tags = get_terms( 'post_tag', array(
    'hide_empty' => false,
      ) );

    filtersRow($cats,$tags);
    single_row();

    if ($query->have_posts()) {

        projects_grid($query,false);
        echo '<div class="no_posts">
        <h4>'.pll__('No posts found').'.</h4>
        </div>';
    } else {
        // no posts
    }
    /* Restore original Post Data */
    wp_reset_postdata();
    echo '</div>';
}
// filter buttons
//////////////////////
// echo '<div class="filters" id="projects_filters">';
// echo '<button  class="p_filter_btn" data-filter="*">' . pll__('Show All') . '</button>';
// foreach ($terms as $term) {
//     $name   = $term->name;
//     $id     = $term->term_taxonomy_id;
//     $filter = '.cat-' . $id;
//
//     echo '<button class="p_filter_btn" data-filter=' . $filter . '>' . $name . '</button>';
// }
// echo '</div>';

// filter  tags buttons
//////////////////////

//   $terms = get_terms( 'post_tag', array(
// 'hide_empty' => false,
//   ) );
//   if ($terms) {
//   echo '<div class="tags_wrapper">';
//
//       foreach($terms as $tag) {
//
//         echo '<span class="p_tag">'. $tag->name . '</span>';
//
//       }
//
//   echo '</div>';
//   }

/// single
 ?>