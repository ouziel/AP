<?php

function exprtiseMobileLayout($id)
{

    if (have_rows('new_service', $id)):
        echo '<div class="expertise-gallery">';
        while (have_rows('new_service', $id)):
            the_row();
            $item   = array(
                'name' => get_sub_field('new_service_name'),
                'info' => get_sub_field('new_service_info'),
                'img' => get_sub_field('new_service_img')
            );
            $result = '<div class="expertise-gallery-cell">';
            $result .= '<div class="expertise_item">
        <div class="img">
        <img src="' . $item['img']['url'] . '"/>
        </div>
        <div class="text">
        <h2 class="title"><span>' . $item['name'] . '</span></h2>
        <div class="info">
        ' . $item['info'] . '
        </div>
        </div>
        </div>';
            $result .= '</div>';

            echo $result;
        endwhile;
        echo '</div>';
    endif;
}


?>
