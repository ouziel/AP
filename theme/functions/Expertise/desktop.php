<?php

// function exprtiseDesktopLayout($id)
// {
//
//     $result = '<section class="blocks_wrapper"><ul>';
//
//     if (have_rows('new_service', $id)):
//         $children = array();
//         while (have_rows('new_service', $id)):
//             the_row();
//             $item = array(
//                 'name' => get_sub_field('new_service_name'),
//                 'info' => get_sub_field('new_service_info'),
//                 'img' => get_sub_field('new_service_img')
//             );
//
//             $result .= '<li class="block_item">
//             <div class="block_img">
//             <img src="' . $item['img']['url'] . '"/>
//             </div>
//             <div class="block_text">
//             <h2><span>' . $item['name'] . '</span></h2>
//             <p>
//             ' . $item['info'] . '
//             </p>
//             </div>
//             </li>';
//             array_push($children, $item);
//         endwhile;
//     else:
//         $result = false;
//     // no rows found
//     endif;
//     $result .= '</ul></section>';
//
//     echo $result;
//
// }
//
function exprtiseDesktopLayout($id)
{

    $result = '<section class="s_wrapper"><ul>';
    if (have_rows('new_service', $id)):
        $children = array();
        while (have_rows('new_service', $id)):
            the_row();
            $item = array(
                'name' => get_sub_field('new_service_name'),
                'info' => get_sub_field('new_service_info'),
                'img' => get_sub_field('new_service_img')
            );

            // $result .= '<li class="s_item">
            // <div class="top">
            //
            // <div class="s_img">
            // <img src="' . $item['img']['url'] . '"/>
            // </div>
            // <div class="s_text">
            // <h2><span>' . $item['name'] . '</span></h2>
            // </div>
            // </div>
            //
            // <div class="bottom">
            // <p>
            // '.$item['info'].'
            // </p>
            // </div>
            // </li>';
            //
            $p_id = rand();
            $result .= '<li class="s_item">
            <svg class="s_item_svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%">
            <defs>
              <pattern   id="pf-'.$p_id.'" width="1" height="1" viewBox="0 0 1 1" preserveAspectRatio="xMidYMid slice">
                <image x="0" y="0" width="1" height="1" xlink:href="'.$item['img']['url'].'"></image>
              </pattern>
              <clipPath id="cp-'.$p_id.'">
                  <rect rx="3" ry="3" x="0" y="-10" width="700px" height="110%" fill="red"></rect>
              </clipPath>

                <linearGradient id="grd-lime">

                  <stop offset="0" stop-color="#082e40" stop-opacity="0.8" />
                  <stop offset="100%" stop-color="#082e40"  stop-opacity="0.9" />
                  </linearGradient>
                  <mask id="m-'.$p_id.'">
                    <rect fill="#fff" x="0" y="0" width="100%" height="100%">
                    </rect>
                    <text font-weight="bold" y="65%" fill="#000" text-anchor="middle" x="50%" font-size="20">
                       '.utf8_strrev($item['name']).'
                    </text>
                  </mask>

              </defs>
              <rect  rx="3" ry="3" x="0" y="-10" width="100%" height="110%" fill="url(#pf-'.$p_id.')" clip-path="url(#cp-'.$p_id.')"></rect>
              <rect class="s_item_line" x="0%" y="42%" height="50" width="100%" fill-opacity="0.9" clip-path="url(#cp-'.$p_id.')" ></rect>
              <text class="s_item_title" x="50%" text-anchor="middle" y="58%" clip-path="url(#cp-'.$p_id.')">'.$item['name'].'</text>
            </svg>

            <div class="s_p_info">
            <h4><span>'.$item['name'].'</span></h4>
              <p>
              '.strip_tags($item['info']).'
              </p>
            </div>
            </li>';
            array_push($children, $item);
        endwhile;
    else:
        $result = false;
    // no rows found
    endif;
    $result .= '</ul></section>';

    echo $result;

}

function svgMaskText() {
  echo '<div class="container">
       <svg class="svg_text_mask">
  <defs>
      <mask id="m1">
        <rect fill="#fff" x="0" y="0" width="100%" height="100%">
        </rect>
        <text font-weight="bold" y="65%" fill="#000" text-anchor="middle" x="50%" font-size="20">
           '.utf8_strrev($name).'
        </text>
      </mask>
     </defs>
      <rect fill="#000" x="0" y="55%" width="100%" height="50" fill-opacity=".7" mask="url(#m1)"/>
    </svg>';
}

?>