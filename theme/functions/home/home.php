<?php

function getHomeProjects() {

$html = '';

$posts = get_field('home_selected_projects');

if ($posts) {
$html       .=  '<section class="home_projects">';
$html       .=  '<h4 class="section_title home_projects_title">';
$html       .=  '<span>'.pll__("Featured Projects").'</span></h4>';

  foreach( $posts as $p ) {

$glry = get_field('projects_gallery', $p->ID);

if (is_array($glry)) {
  if (isset($glry[0]['url'])) {
    $img = $glry[0]['url'];

  }
  else {
  $img = 'no-img1';
}
if (!$img) {
  $img = 'no-img2';
}
}
else {
$img = 'no-img3';
}

$html         .= 	 '<article class="project">';
$html         .=   '<img src="'.$img.'"  />';
$html         .=   '<a href="'.get_permalink( $p->ID ).'">';
$html         .=   '<span>'.get_the_title( $p->ID ).'</span></a>';
$html         .=   '</article>';
}

    $html         .=   '</section>';
    echo $html;
}


}
?>