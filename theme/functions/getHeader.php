<?php

function header_text() {
  $desc = 'קבלני חשמל ותקשורת';
  $name = get_bloginfo('name');
echo '<div class="home_header_text">';
echo '<h1>'.$name.'</h1>';
echo '<h3>'.$desc.'</h3>';
echo '</div>';
}
function getHeader()
{

    /// if page is ghomepage

    if (is_home() || is_front_page()) {

        echo '<header class="home">';

        $image1 = get_field('home_slider_img_1');
        $image2 = get_field('home_slider_img_2');
        $image3 = get_field('home_slider_img_3');
        $image4 = get_field('home_slider_img_4');

        $images = array(
            ''
        );

        array_push($images, $image1);
        array_push($images, $image2);
        array_push($images, $image3);
        array_push($images, $image4);

        flickityHomeSlider($images);
        header_text();
        echo '</header>';


    } else {


        // cover header
        echo '<header class="page-header">';

        $classes = array();
        $title   = get_the_title();
        array_push($classes, 'cover');

        if (wp_is_mobile()) {
            //background-color css class
            array_push($classes, 'cover_mobile');
            echo '<div class="' . join(' ', $classes) . '">';
            echo '<h5>'. $title .'</h5>';
            echo '</div>';
        } else {
            //background-image css class
            array_push($classes, 'cover_desktop');

            $default_cover = get_field('options_default_cover_img', 'option');
            $img           = get_field('page_cover');
            if ($img) {
                $img = $img['url'];
            } else {
              $img = $default_cover['url'];
            }
            if (is_post_type_archive('Projects')){
              $img = $default_cover['url'];
            }
            echo '<div style="background-image:url(' . $img . ')" class="' . join(' ', $classes) . '">';
            echo '</div>';
        }

        echo '</header>';
    }

}

function flickityHomeSlider($images)
{
  echo '<div class="home-gallery">';

    foreach ($images as $img) {
        if (!empty($img)) {
            echo '<div class="gallery-cell">';
            echo '<img src="' . $img['url'] . '"  alt="' . $img['alt'] . '"/>';
            echo '</div>';
        }
    }
      echo '</div>';
}
?>