<?php


function ap_search() {

  $type               = 'Projects';
  $ppp                = 16;
  $search_input       = '';
  $category_input     = '';
  $page_input         = 1;

  $search_input       = $_POST['search_input'];
  $category_input     = str_replace('cat-','',$_POST['category_input']);
  if ($category_input === 1 || $category_input === '1') {
    $category_input = '';
  }
  $tags_input         = str_replace('tag-','',$_POST['tag_input']);
  if ($tags_input === 1 || $tags_input === '1') {
    $tags_input = '*';
  }

  $page_input         = $_POST['page_input'];
  $offset             = (((int) $page_input) - 1) * $ppp;

  $args               = array(
                          'post_type' => array(
                             $type,
                          ),
                          'posts_per_page' => $ppp,
                          'offset' => $offset,
                          'tag__in' => ($tags_input === '*') ? '' : array($tags_input),
                          's' => $search_input,
                          'cat' => $category_input,
                      );

  $query                = new WP_Query($args);

// echo $tags_input;

do_action('projects_search',$query,true);
// echo json_encode($query);
die();
}


add_action( 'wp_ajax_ap_search', 'ap_search' );
add_action( 'wp_ajax_nopriv_ap_search', 'ap_search' );
?>