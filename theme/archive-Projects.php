<?php

get_header();
?>


<main id="stage" class="container shapes">
  <div class="page_title">

    <h5 class="section_title">  <span><?php pll_e('Projects'); ?></span></h5>
  </div>
  <!-- /// single item setup
  /////////////////////// -->

<section class="projects_gallery_wrapper">
<?php
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args  = array(
    'post_type' => array(
        'Projects',
    ),
    'posts_per_page' => '9',
     'paged'         => $paged,
);
$query = new WP_Query($args);

$terms = get_terms(array(
    'taxonomy' => 'category',
    'hide_empty' => false
));

do_action('projects_archive',$terms, $query);

// if (wp_is_mobile()) {
//
//   do_action('projects_archive_mobile', $terms, $query);
//   // do_action('projects_archive_desktop', $terms, $query);
//
// } else {
//
//   // do_action('projects_archive_mobile', $terms, $query);
//   do_action('projects_archive_desktop', $terms, $query);
//
// }

?>

</main>
<?php
get_footer();
?>
