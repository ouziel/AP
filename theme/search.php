<?php
/*
Template Name: Search Page
*/
get_header(); ?>

<main class="container">

<?php
global $query_string;

$query_args = explode("&", $query_string);
$search_query = array(
  'post_type' => 'Projects',
  'posts_per_page' => 9
);

if( strlen($query_string) > 0 ) {
	foreach($query_args as $key => $string) {
		$query_split = explode("=", $string);
		$search_query[$query_split[0]] = urldecode($query_split[1]);
	} // foreach
} //if

$query = new WP_Query($search_query);

$terms = get_terms(array(
    'taxonomy' => 'category',
    'hide_empty' => false
));

do_action('projects_archive',$terms, $query);


// echo '<pre>';
// var_dump($posts);
// echo '</pre>';

 ?>

</main>


<?php get_search_form( true ); ?>
<?php get_footer(); ?>

<!--
initial projects page load

show 9 projects maximum
pick from featured pool first
then add if needed


any filter action triggers a from
that form send a query to wp/?s=query&post_type=type&cat=cat&tag=tag

then i get a response

i need to know how to translate that response
to an array
with 2 arrays in it
of 9 results/projects each -->