<?php

if (function_exists('pll_register_string')) { 
pll_register_string('Full Name', 'Full Name');
pll_register_string('Email Address', 'Email Address');
pll_register_string('Phone Number', 'Phone Number');
pll_register_string('Company Name', 'Company Name');
pll_register_string('Message', 'Message');
pll_register_string('Send', 'Send');
pll_register_string('Eginstein-Pines', 'Eginstein-Pines');
pll_register_string('Directions', 'Directions');
pll_register_string('TEL', 'TEL');
pll_register_string('ADDRESS', 'ADDRESS');
pll_register_string('EMAIL', 'EMAIL');
pll_register_string('FAX', 'FAX');
pll_register_string('Contact Information', 'Contact Information');
pll_register_string('Get in Touch', 'Get in Touch');
pll_register_string('Your message was successfuly saved.', 'Your message was successfuly saved.');
pll_register_string("We'll be in touch soon", "We'll be in touch soon");
pll_register_string("click to see directions in google maps", "click to see directions in google maps");
pll_register_string("click to send us an email", "click to send us an email");
pll_register_string("click to call", "click to call");
pll_register_string("Leave a message", "Leave a message");
pll_register_string("Show All", "Show All");
pll_register_string("Categories", "Categories");
pll_register_string("Sectors", "Sectors");
pll_register_string("Featured Projects", "Featured Projects");
pll_register_string("Projects", "Projects");
pll_register_string("All Projects", "All Projects");
pll_register_string("Parent Item", "Parent Item");
pll_register_string("All Items", "All Items");
pll_register_string("Add New", "Add New");
pll_register_string("New New", "New New");
pll_register_string("Tags", "Tags");
pll_register_string("Search", "Search");
pll_register_string("Pages", "Pages");
pll_register_string("Submit", "Submit");
pll_register_string("No posts found", "No posts found");
pll_register_string("Read more", "Read more");
pll_register_string("Project Page", "Project Page");
pll_register_string("Expertise", "Expertise");
pll_register_string("Team", "Team");
pll_register_string("Certifications", "Certifications");
pll_register_string("Summary", "Summary");
pll_register_string("Profile", "Profile");
pll_register_string("About", "About");
pll_register_string("Profile", "Profile");
pll_register_string("Management", "Management");
}
?>